<?php

namespace Gupo\MiddleOfficeMc\App;

use Gupo\MiddleOffice\Utils\Utils;
use Gupo\MiddleOffice\Config\Config;
use Gupo\MiddleOffice\Clients\Client;
use Gupo\MiddleOffice\VO\RequestHeader;
use GuzzleHttp\Exception\GuzzleException;
use Gupo\MiddleOfficeMc\Error\CenterErrorInfo;
use Gupo\MiddleOffice\Exception\ClientException;
use Gupo\MiddleOfficeMc\Exception\CenterException;

/**
 * Class MessageCenter
 *
 * @author: Wumeng - wumeng@gupo.onaliyun.com
 * @since: 2023-06-15 16:42
 */
class MessageCenterClient extends Client
{
    /**
     * @throws ClientException
     */
    public function __construct()
    {
        parent::__construct(new Config());
    }

    /**
     * 发送消息
     *
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-06-28 16:58
     */
    public function sendMsg($body, $endpoint, array $headerExtra = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/send');
    }

    public function sendCardMsg($body, $endpoint, array $headerExtra = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/send-card-msg');
    }

    /**
     * 查询消息发送状态
     *
     * @param $messageUUID
     * @param $endpoint
     * @param  array  $extra
     * @param  array  $headerExtra
     * @return mixed
     * @throws CenterException
     * @throws ClientException
     * @throws GuzzleException
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-06-28 17:33
     */
    public function queryMessageResult($messageUUID, $endpoint, array $extra = [], array $headerExtra = [])
    {
        if (Utils::isUnset($messageUUID) || Utils::empty_($messageUUID)) {
            throw new CenterException(CenterErrorInfo::MISSING_MESSAGE_UUID);
        }
        $body = array_merge(['uuid' => $messageUUID], $extra);
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/sendStatus');
    }

    /**
     * 获取站内信列表  v1版本
     * @param $channelUserSign
     * @param $endpoint
     * @param array $extra
     * @param array $headerExtra
     * @return mixed
     * @throws CenterException
     */

    public function getInternalMsgList($channelUserSign, $endpoint, array $extra = [], array $headerExtra = [])
    {
        if (Utils::isUnset($channelUserSign) || Utils::empty_($channelUserSign)) {
            throw new CenterException(CenterErrorInfo::MISSING_CHANNEL_USER_SIGN);
        }
        $body = array_merge(['channel_user_sign' => $channelUserSign], $extra);
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/list');
    }


    /**
     * 获取站内信列表  v2版本
     *
     * @param array $body
     * @param string $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws CenterException
     * @throws ClientException
     * @throws GuzzleException
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-06-28 17:37
     */

    public function getInternalMsgListV2(array $body, $endpoint, array $headerExtra = [])
    {

        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/list-v2');
    }


    /**
     *使用端 家医(国瑞) 目标是实现发送端查看接收人消息是否已读
     *实现方式 通过uuid进行whereIn查询  再根据uuid对应传参的userid和channel_user_sign 查询已读表
     *
     * @param array $body
     * body 示例
     *{
     *  "94c929aa56e946648100e9accad9a0c2": {
     *      "channel_user_sign": "15838952986",
     *      "userid": "15838952986"
     *   }
     * }
     *body 参数说明  key 为 消息返回的uuid  channel_user_sign唯一标识  userid已读标识
     * @param $endpoint
     * @param array $headerExtra
     * @return mixed
     */
    public function getSourceInternalMsgList(array $body, $endpoint, array $headerExtra = [])
    {

        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/batch-list');
    }



    /**
     * 批量发送同一模板不同消息
     *
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-07-13 14:06
     */
    public function sendBatchDiffMsg($body, $endpoint, array $headerExtra = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/send-batch-diff');
    }

    /**
     * 批量发送同一模板相同消息
     *
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-07-13 14:06
     */
    public function sendBatchSameMsg($body, $endpoint, array $headerExtra = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/send-batch-same');
    }

    /**
     * 获取站内信消息详情
     *
     * @param $uuid
     * @param $channelUserSign
     * @param $endpoint
     * @param  array  $headerExtra
     * @param  array  $extra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-07-25 10:21
     */
    public function getInternalMsgDetail($uuid, $channelUserSign, $endpoint, array $headerExtra = [], array $extra = [])
    {
        $body = array_merge(['channel_user_sign' => $channelUserSign, 'uuid' => $uuid], $extra);
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/detail');
    }

    /**
     * 获取站内信消息数量
     *
     * @param $channelUserSign
     * @param $endpoint
     * @param  array  $headerExtra
     * @param  array  $extra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-07-25 10:23
     */
    public function getInternalMsgCount($channelUserSign, $endpoint, array $headerExtra = [], array $extra = [])
    {
        $body = array_merge(['channel_user_sign' => $channelUserSign], $extra);
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/count');
    }
    public function getInternalMsgCountV2($channelUserSign, $endpoint, array $headerExtra = [], array $extra = [])
    {
        $body = array_merge(['channel_user_sign' => $channelUserSign], $extra);
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/count-v2');
    }

    /**
     * 站内信消息变更状态为已读
     *
     * @param $channelUserSign
     * @param $batchUuid
     * @param $clearAll
     * @param $endpoint
     * @param  array  $headerExtra
     * @param  array  $extra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-07-25 10:42
     */
    public function readInternalMsg($channelUserSign, $batchUuid, $clearAll, $endpoint, array $headerExtra = [], array $extra = [])
    {
        $body = array_merge(['channel_user_sign' => $channelUserSign, 'batch_uuid' => $batchUuid, 'clear_all' => $clearAll], $extra);
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/read-msg');
    }

    public function readInternalMsgV2($body, $endpoint, array $headerExtra = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/read-msg-v2');
    }

    /**
     * 发送钉钉消息
     *
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-11-08 10:00
     */
    public function sendDing($body, $endpoint, array $headerExtra = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/send-ding');
    }

    /**
     * 发送专有钉消息
     *
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     * @author Wumeng wumeng@gupo.onaliyun.com
     * @since 2023-11-08 09:59
     */
    public function sendExcDing($body, $endpoint, array $headerExtra = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint . 'innerapi/v1/message/send-exc-ding');
    }


    /**
     * 站内信模板-创建站内信模板
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function createInternalTemplate($body, $endpoint, array $headerExtra = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/template/create-internal-template');
    }

    /**
     * 站内信模板-修改站内信模板
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function changeInternalTemplate($body, $endpoint, array $headerExtra = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/template/change-internal-template');
    }


    /**
     * 站内信模板-获取站内信模板列表
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function listInternalTemplate($body, $endpoint, array $headerExtra = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/template/list-internal-template');
    }

    /**
     * 站内信模板-删除站内信模板
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function destroyInternalTemplate($body, $endpoint, array $headerExtra = [])
    {
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/template/destroy-internal-template');
    }

    /**
     * 消息类型-创建消息类型
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function createMsgType($body, $endpoint, array $headerExtra = []){
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/msg-type/create');
    }


    /**
     * 消息类型-获取消息类型列表
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function listMsgType($body, $endpoint, array $headerExtra = []){
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/msg-type/list');
    }


    /**
     * 钉钉消息-修改钉钉OA消息状态
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function changeOAMsgStatus($body, $endpoint, array $headerExtra = []){
        $header = new RequestHeader($this->config, $body, $this->config->appId);

        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/message/read-oa-msg');
    }

    /**
     * 短信预览和校验
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function messagePreview($body, $endpoint, array $headerExtra = []){
        $header = new RequestHeader($this->config, $body, $this->config->appId);
        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/message/preview');
    }


    /**
     * 获取模板列表
     * @param $body
     * @param $endpoint
     * @param  array  $headerExtra
     * @return mixed
     * @throws ClientException
     * @throws GuzzleException
     */
    public function templateList($body, $endpoint, array $headerExtra = []){
        $header = new RequestHeader($this->config, $body, $this->config->appId);
        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/template/list');
    }


    /**
     * 获取待发送列表
     * @apiParam {Number} [page] 页码
     * @apiParam {Number} [per_page] 条数
     * @apiParam {String} [channel_user_sign] 唯一标识
     * @apiParam {String} [send_status] 发送状态 0待发送 1成功 2失败 3待回调
     * @apiParam {String} [template_type] 2短信 3钉钉 4专有顶顶
     */
    public function getIsAwaitMessageList($body, $endpoint, array $headerExtra = []){
        $header = new RequestHeader($this->config, $body, $this->config->appId);
        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/message/await-message');
    }

    /**
     * 待发送列表-立即发送
     * @apiHeader {String} UserToken 用户中心token
     *
     * @apiParam {Array} uuid 消息的uuid
     */
    public function immediatelySend($body, $endpoint, array $headerExtra = []){
        $header = new RequestHeader($this->config, $body, $this->config->appId);
        return $this->callApiPost($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/message/immediately_send');
    }


    /**
     * 获取所有站内信发送列表
     * @apiParam {Number} [page] 页码
     * @apiParam {Number} [per_page] 条数
     * @apiParam {String} [message_type_id] 消息类型id
     * @apiParam {String} [template_id] 模板id
     * @apiParam {String} [channel_user_sign] 唯一标识
     */
    public function internalList($body, $endpoint, array $headerExtra = []){
        $header = new RequestHeader($this->config, $body, $this->config->appId);
        return $this->callApiGet($header->getHeader($headerExtra), $body, $endpoint. 'innerapi/v1/message/internal-list');
    }



}
